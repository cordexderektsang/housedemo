﻿using UnityEngine;
using System.Collections;

public class MoveCam : MonoBehaviour {


	public GameObject tangoCam;
	public Camera cam;
	private bool startFollowCam = false;
	private Vector3 position;
	// Use this for initialization
	
	// Update is called once per frame
	void Update () 
	{
		if (!startFollowCam) {
			return;
		}

		this.transform.position = tangoCam.transform.position;
		this.transform.localEulerAngles = tangoCam.transform.localEulerAngles;
	
	}

	public void StartFollowCam ()
	{
		startFollowCam = true;
		cam.enabled = false;
		this.transform.position = tangoCam.transform.position;
		this.transform.localEulerAngles = tangoCam.transform.localEulerAngles;
		//tangoCam.SetActive (false);
		this.gameObject.SetActive (true);
	}

/*	void OnTriggerStay(Collider other) {
		Debug.Log ("stay");
		position = this.transform.position;
		startFollowCam = true;
	}

	void OnTriggerExit(Collider other) {
		//Debug.Log ("exit");
		startFollowCam = false;
	//	this.transform.position = position;

	}

	void OnTriggerEnter(Collider other) {
		Debug.Log ("enter");
		startFollowCam = true;

	}*/
}
