﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayAnimationController : MonoBehaviour {

	public List<GameObject> playingAnimationButtonList;

 

	private Coroutine reversetCorotine;

	private Coroutine resetCorotine;

	private AnimationProperties animationProperties;

    public List<OnAnimationEnd> onAnimationEndList;
	public delegate void OnAnimationEnd(GameObject button);
	public bool isAnyAnimationPlaying = false;

	void Awake()
	{
		playingAnimationButtonList = new List<GameObject> ();

	}



	public void UpdateAnimationList()
	{
		for(int i = 0; i < playingAnimationButtonList.Count; i++)
		{
		    if (playingAnimationButtonList[i].GetComponentInParent<AnimationProperties>() == null)
            {
                playingAnimationButtonList[i].gameObject.SetActive(true);
                playingAnimationButtonList.RemoveAt(i);
            }

            else if (!playingAnimationButtonList[i].GetComponentInParent<AnimationProperties>().isAnimating)
            {
                playingAnimationButtonList[i].gameObject.SetActive(true);
				//showButton(playingAnimationButtonList[i]);

				//playingAnimationButtonList[i].GetComponent<ThreeDObjectButton>().ShowObject();
//				if (vr3DMovement != null) {
//					for (int j = 0; j < vr3DMovement.GetPrevAreaData ().enableObjectList.Count; j++) {
//						if (playingAnimationButtonList [i].Equals (vr3DMovement.GetPrevAreaData ().enableObjectList [j])) {
//							playingAnimationButtonList [i].gameObject.SetActive (false);
//						}
//					}
//				} else {
//					if(true)
//					{
//					}
//					playingAnimationButtonList [i].gameObject.SetActive (false);
//				}

				playingAnimationButtonList.RemoveAt(i);

				//onAnimationEndList [i] (playingAnimationButtonList [i]);

			}
		}
	}
    /// <summary>
    /// play current object's parent animation
    /// </summary>
    public void PlayParentAnimation(GameObject button)
    {
		if (button = null) {
			return;
		}
        Animation animation = button.GetComponentInParent<Animation>();
        AnimationProperties animationProperties = button.GetComponentInParent<AnimationProperties>();
		//AnimationProperties animationProperties = button.GetComponentInParent<AnimationProperties>();

        //if (!animation.isPlaying)
        if (!animationProperties.isAnimating)
        {
            handleAnimation(animation, animationProperties);
        }
        // hide the button
        button.SetActive(false);
        // add the button to the animation playing list
        playingAnimationButtonList.Add(button);

        //hideButton (button);
    }
    /// <summary>
    /// play current object's parent animation
    /// </summary>
    public void PlayAnimation(GameObject animationObject)
    {
		if (animationObject = null) {
			return;
		}
		isAnyAnimationPlaying = true;
        Animation animation = animationObject.GetComponent<Animation>();
		animationProperties = animationObject.GetComponent<AnimationProperties>();
		//CCScreenLogger.LogStatic ("hit : " + animationObject.name, 2);
		//CCScreenLogger.LogStatic ("hit : " + animationProperties.isAnimating, 5);
        //if (!animation.isPlaying)
        if (!animationProperties.isAnimating)
        {
            handleAnimation(animation, animationProperties);
        }
    }



    /// <summary>
    /// to play animaton
    /// </summary>
    /// <param name="animation"></param>
    /// <param name="animationProperties"></param>
    private void handleAnimation(Animation animation, AnimationProperties animationProperties)
	{
		if (animation == null || animationProperties ==null) {
			return;
		}
		string animationStateName = animation.clip.name;
		animation.Play();
		animationProperties.isAnimating = true;
		if (animationProperties.IsReversible)
		{
			reversetCorotine = StartCoroutine(reverseAnimation(animation, animationProperties, animationStateName));
		}
		else
		{
			resetCorotine = StartCoroutine(resetAnimation(animation, animationProperties, animationStateName));
		}
	}

	/// <summary>
	/// Reverses the animation.
	/// </summary>
	/// <param name="animation">Animation.</param>
	/// <param name="animationProperties">Animation Properties</param>
	/// <param name="animationStateName">Animation state name.</param>
	private IEnumerator reverseAnimation(Animation animation, AnimationProperties animationProperties, string animationStateName)
	{
		if (animation == null || animationProperties ==null) {
			yield break;
		}
		float delayTime = animation[animationStateName].length;
		yield return new WaitForSeconds(delayTime);
		animation.Stop();
		animation[animationStateName].speed = animation[animationStateName].speed * - 1;
		if (animation[animationStateName].speed == 1)
		{
			animation[animationStateName].time = 0;
		}
		else
		{
			animation[animationStateName].time = animation[animationStateName].length;
		}
		animationProperties.isAnimating = false;
		isAnyAnimationPlaying = false;
	}

	/// <summary>
	/// Reset the animation.
	/// </summary>
	/// <param name="animation">Animation.</param>
	/// <param name="animationProperties">Animation Properties</param>
	/// <param name="animationStateName">Animation state name.</param>
	private IEnumerator resetAnimation(Animation animation, AnimationProperties animationProperties, string animationStateName)
	{
		float delayTime = animation[animationStateName].length;
		yield return new WaitForSeconds(delayTime);
		animation [animationStateName].speed = 1;
		animationProperties.isAnimating = false;
        animation[animationStateName].time = 0;
        animation.Stop();
		isAnyAnimationPlaying = false;
    }


	public void stopReverseAnimationCoroutine (){

		if(reversetCorotine != null){
			StopCoroutine (reversetCorotine);
			if (animationProperties != null) {
				animationProperties.isAnimating = false;
			}
		}

	}


	public void stopResetAnimationCoroutine (){

		if(resetCorotine != null){
			StopCoroutine (resetCorotine);
			if (animationProperties != null) {
				animationProperties.isAnimating = false;
			}
		}

	}	

}
